
// Get data from api and create contact object
function createContact(url) {

    // create new promise
    return new Promise((resolve, reject) => {

        // create get method to the desired api
        axios.get(url).then(response => {

            let contacts = []

            // check if response have data
            if(response.data.length) {

                response.data.forEach(item => {

                    // create contact object
                    let contact = {

                        username: item.username ? item.username : [],

                        email: {

                            value: item.email ? item.email : [],

                            link: item.email ? 'mailto:' + item.email : []                      
                        },

                        street: {

                            value: item.address.street,

                            link: 

                                item.address.city && item.address.street

                                    ? 'https://www.google.com/maps/search/' + item.address.city + ' ' +  item.address.street

                                    : 'https://www.google.de/maps/place/' + item.address.geo.lat + ',' + item.geo.lng
                        },

                        city: item.address.city ? item.address.city : [],

                        company: item.company.name ? item.company.name : [],

                        catchPhrase: item.company.catchPhrase ? item.company.catchPhrase : [],
                    }

                    contacts.push(contact)
                })

                resolve(contacts) 
            
            } else {

                return reject('No contacts !')
            } 
        })
    })
}

// Create row elements and append to the view.
function createHTMLRow(table, data) {

	data.forEach(item => {

		let tr = document.createElement('tr')

		for(let [key, data] of Object.entries(item)) {

			let td = document.createElement('td')

			if(key == 'email' || key == 'street') {

				td.appendChild(createLink(data.link, data.value))

			} else {

				td.innerHTML = data
			}

			tr.appendChild(td)

			document.querySelector(table).appendChild(tr)
		}
	})
}

// Create link node
function createLink(href, html) {

	let link = document.createElement('a')

	link.setAttribute('href', href)

	link.setAttribute('target', '_blank')

	link.innerHTML = html

	return link
}

window.onload = function(event) { 

    createContact('https://jsonplaceholder.typicode.com/users').then(response => {

        // wait for response than create table
        createHTMLRow('#datatable tbody', response)
    
    }).catch(err => {
        
        console.log(err)
    })
}

